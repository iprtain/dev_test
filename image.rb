class Image
  attr_accessor :dimensions, :color, :board, :pixels
  def initialize(m, n)
    @dimensions = {M: m.to_i, N: n.to_i}
    @mcoord = (1..dimensions[:M]).to_a
    @ncoord = (1..dimensions[:N]).to_a
    @color = "O"
    @board = make_board  # list of coordinates
    @pixels = add_color   #hash with coordinates as key and color as value
    @drawing = []   #final output, all values pushed to list

  end

  def make_board
    pixels = []
          @ncoord.each do |n|
            @mcoord.each do  |m|

                pixels.push([m,n])
              end
            end
            return pixels
          end

def add_color
  pixels = {}
  @board.each do |position|
    pixels[position] = @color
  end
  return pixels
end


  def draw
    @drawing = []
    @pixels.each do |key, value|
      @drawing.push(value)
    end
    count = 0
    @drawing.each do |item|
      count += 1
      if count/@dimensions[:M] == 1
        count = 0
      puts item
    else
      print item
    end


    end

  end
end
