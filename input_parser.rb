require "./image.rb"
require "./actions/create_image.rb"
require "./actions/clear.rb"
require "./actions/color_pixel.rb"
require "./actions/vertical_line.rb"
require "./actions/horizontal_line.rb"
require "./actions/fill.rb"


class InputParser
  attr_accessor :command_type

  def initialize(input)
    parsed = input.split(" ")
    parsed.map! {|num| num.to_i !=0 ? num.to_i : num}
    @command_type = parsed[0]
    @arg1 = parsed[1]
    @arg2 = parsed[2]
    @arg3 = parsed[3]
    @arg4 =  parsed[4]


    if @command_type == "I"
      create_image(@arg1, @arg2)

    elsif @command_type == "C"
      clear()

    elsif @command_type == "L"
      color_pixel(@arg1, @arg2, @arg3)

    elsif @command_type == "X"
      exit

    elsif @command_type == "S"
      $image.draw

    elsif @command_type == "V"
      vertical_line(@arg1, @arg2, @arg3, @arg4)

    elsif @command_type == "H"
      horizontal_line(@arg1, @arg2, @arg3, @arg4)

    elsif @command_type == "F"
      fill(@arg1, @arg2, @arg3)

    end





  end
end
