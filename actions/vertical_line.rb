def vertical_line(x, y1, y2, color)
  if y1 < y2
    range = (y1..y2).to_a
  else
    range = (y2..y1).to_a
  end

  range.each do |y|
    $image.pixels[[x, y]] = color
  end
end
