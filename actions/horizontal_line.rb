def horizontal_line(x1, x2, y, color)
  if x1 < x2
    range = (x1..x2).to_a
  else
    range = (x2..x1).to_a
  end

  range.each do |x|
    $image.pixels[[x, y]] = color
  end
end
