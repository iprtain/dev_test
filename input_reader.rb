class InputReader
  attr_accessor :input

  def initialize
    @input = gets.chomp.upcase
  end

end
